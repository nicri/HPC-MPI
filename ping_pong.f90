PROGRAM hello_world_mpi
    include 'mpif.h'
    integer, parameter :: MK = kind(1.0D0)
    integer :: process_Rank, size_Of_Cluster, ierror, i, N, msg_size
    real(MK) :: ball=1, t1, t2
    INTEGER, DIMENSION(MPI_STATUS_SIZE) :: status
    REAL(MK), DIMENSION(10) :: Bandwidths
    REAL(MK), DIMENSION(2**18) :: big_ball
    
    call MPI_INIT(ierror)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, size_Of_Cluster, ierror)
    call MPI_COMM_RANK(MPI_COMM_WORLD, process_Rank, ierror)

    msg_size = 1!size(big_ball)

    N=10
    DO i=1,N
        t1 = MPI_WTIME()
        IF(process_Rank .EQ. 1) THEN 
            call MPI_RECV(ball, msg_size, MPI_DOUBLE_PRECISION, 0, 1, MPI_COMM_WORLD,status, ierror)       
            call MPI_SEND(ball, msg_size, MPI_DOUBLE_PRECISION, 0, 2, MPI_COMM_WORLD, ierror)
        ELSE if(process_Rank .EQ. 0) THEN
            !PRINT*, 'Process ', process_Rank,' has the ball'
            call MPI_SEND(ball, msg_size, MPI_DOUBLE_PRECISION, 1, 1, MPI_COMM_WORLD, ierror)
            call MPI_RECV(ball, msg_size, MPI_DOUBLE_PRECISION, 1, 2, MPI_COMM_WORLD,status, ierror)
        ENDIF
        call MPI_BARRIER(MPI_COMM_WORLD,ierror)
        t2 = MPI_WTIME()
        Bandwidths(i) = sizeof(ball)/(t2-t1)
    END DO
    PRINT*, 'Size of message: ', sizeof(ball)
    PRINT*, 'Average Time: ', process_Rank, sizeof(ball)/(SUM(Bandwidths)/SIZE(Bandwidths))
    PRINT*, 'Min Time: ', process_Rank, sizeof(ball)/MAXVAL(Bandwidths)
    PRINT*, 'Max Time: ', process_Rank, sizeof(ball)/MINVAL(Bandwidths)
    PRINT*, 'Average Bandwidth rank: ', process_Rank, SUM(Bandwidths)/SIZE(Bandwidths)
    PRINT*, 'Min Bandwidth rank: ', process_Rank,MINVAL(Bandwidths)
    PRINT*, 'Max Bandwidth rank: ', process_Rank,MAXVAL(Bandwidths)



    call MPI_FINALIZE(ierror)
END PROGRAM

