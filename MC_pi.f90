PROGRAM MC_pi
    use mpi
    integer, parameter :: MK = selected_real_kind(11)
    integer :: i, N, k, seedsize, counter_convergence = 0, counter_proc = 0, tot_loop, N_per_proc
    INTEGER, DIMENSION(:), ALLOCATABLE :: seed
    !INTEGER :: count1,count2, count_rate
    real(MK) :: sum_square=0, sum_circle=0, global_square, global_circle, my_pi, my_pi_temp, tolerance
    real(MK) :: t1, t2, max_time
    real(MK), DIMENSION(2,1000) :: rand
    real(MK), DIMENSION(2) :: local, global
    integer :: process_Rank, size_Of_Cluster, ierror
    INTEGER, DIMENSION(MPI_STATUS_SIZE) :: status
    
    call MPI_INIT(ierror)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, size_Of_Cluster, ierror)
    call MPI_COMM_RANK(MPI_COMM_WORLD, process_Rank, ierror)

    N=1600000
    tolerance = 1e-7
    N_per_proc = N/size_Of_Cluster
    !Initialize a different random seed for each processor 
    CALL RANDOM_SEED(SIZE=seedsize)
    ALLOCATE(seed(seedsize))
    DO k=1,seedsize
        seed(k) = 1238239 + process_Rank*8365
    ENDDO
    CALL RANDOM_SEED(PUT=seed)

    my_pi_temp = 0
    my_pi = 0
    !Start the clock
    t1 = MPI_WTIME()
    loop: DO i=1,N_per_proc
        CALL RANDOM_NUMBER(rand)
        !rand has 1000 (x,y) random points
        DO k=1,size(rand,2)
            counter_proc = counter_proc+1
            IF(rand(1,k)**2+rand(2,k)**2 .LE. 1_MK) THEN
                sum_circle = sum_circle + 1
            ELSE 
                sum_square = sum_square + 1
            ENDIF
        ENDDO
        local = (/ sum_circle, sum_square /)
        global = (/ global_circle, global_square /)
        call MPI_ALLREDUCE( local, global, 2, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ierr )
        my_pi = 4_MK*(global(1)/(global(1)+global(2)))
        !Check convergence every 1000 points
        IF(abs(my_pi-my_pi_temp) .LE. tolerance) THEN
            counter_convergence = counter_convergence + 1
            !just to be sure to not randomly obtain similar wrong values, the grater the less probable
            If(counter_convergence .GE.20) THEN 
                !IF(process_Rank .EQ. 0) PRINT*, 'Number of iterations to convergence: ', i
                EXIT loop
            ENDIF
        ENDIF
        my_pi_temp = my_pi
    ENDDO loop

    !Stop the clock
    t2 = MPI_WTIME()
    !Measure the MAX time
    call MPI_ALLREDUCE(t2-t1, max_time, 1, MPI_DOUBLE_PRECISION, MPI_MAX, MPI_COMM_WORLD, ierr )
    call MPI_ALLREDUCE(counter_proc, tot_loop, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ierr )
    !if(process_Rank .EQ. 0) PRINT*, 'Executed in: ', max_time, ' s'
    if(process_Rank .EQ. 0) PRINT*, max_time, tot_loop
    !IF(process_Rank .EQ. 0) PRINT*, 'My estimate of pi: ',my_pi
    !IF(process_Rank .EQ. 0) PRINT*, 'Previous step    : ', my_pi_temp
   
    !PRINT *, 'Processor ', process_Rank, ' made ', counter_proc, 'out of', tot_loop
    call MPI_FINALIZE(ierror)
END PROGRAM

!SEQUENTIAL CODE
! PROGRAM MC_pi
!     !include 'mpif.h'
!     integer, parameter :: MK = kind(1.0D0)
!     integer :: i, N
!     INTEGER :: count1,count2, count_rate
!     real(MK) :: sum_square=0, sum_circle=0
!     real(MK), DIMENSION(2) :: rand
!     !INTEGER, DIMENSION(MPI_STATUS_SIZE) :: status

!     N=37566000*4
!     CALL SYSTEM_CLOCK(count1,count_rate)
!     CALL CPU_TIME(t1)
!     DO i=1,N
!         CALL RANDOM_NUMBER(rand)
!         IF(norm2(rand) .LE. 1_MK) THEN
!             sum_circle = sum_circle + 1
!         ELSE 
!             sum_square = sum_square + 1
!         ENDIF
!     ENDDO
!     CALL CPU_TIME(t2)
!     CALL SYSTEM_CLOCK(count2)
!     PRINT*,'Calculation done in CPU time:',t2-t1,'s and wall time: ', (REAL(count2)-REAL(count1))/REAL(count_rate),'s'
!     PRINT*, 4_MK*(sum_circle/(sum_circle+sum_square))
    
! END PROGRAM