PROGRAM hello_world_mpi
    include 'mpif.h'
    
    integer :: process_Rank, size_Of_Cluster, ierror
    character(LEN=10) :: processor_name 
    
    call MPI_INIT(ierror)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, size_Of_Cluster, ierror)
    call MPI_COMM_RANK(MPI_COMM_WORLD, process_Rank, ierror)
    call MPI_GET_PROCESSOR_NAME(processor_name, len, ierror)

    IF(process_Rank==0) PRINT*, 'Hello World from rank ', process_Rank
    DO i = 0,3
        IF(i == process_Rank) THEN
            WRITE(*,*) 'Process ', processor_name,' is rank ',process_Rank, 'of ', size_Of_Cluster
        END IF
        call MPI_BARRIER( MPI_COMM_WORLD, ierror)
    END DO
    
    call MPI_FINALIZE(ierror)
END PROGRAM