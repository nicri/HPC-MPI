reset

# wxt
#set terminal wxt size 410,250 enhanced font 'Verdana,9' 
# png
set terminal pngcairo size 800,600 
set output 'latency.png'
# svg
#set terminal svg size 410,250 fname 'Verdana, Helvetica, Arial, sans-serif' \
#fsize '9' rounded dashed
#set output 'nice_web_plot.svg'

# define axis
# remove border on top and right and set color to gray
#set style line 11 lc rgb '#808080' lt 1
set border 3 back
set tics nomirror
# define grid
set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12

# color definitions
set style line 1 lc rgb '#8b1a0e' pt 1 ps 1.5 lt 1 lw 2 # --- red
set style line 2 lc rgb '#5e9c36' pt 6 ps 1.5 lt 1 lw 2 # --- green
set style line 3 lc rgb '#000099' pt 7 ps 1.5 lt 1 lw 2 # --- blu

set key bottom right

set xlabel 'size of msg [bytes]'
set ylabel 'Sending Time [s]'
set logscale xy
# set xrange [0:1]
# set yrange [0:1]

plot 'latency.dat' u 1:2 t "Minimum",\
    '' u 1:3 t "Average",\
    '' u 1:4 t "Max",
